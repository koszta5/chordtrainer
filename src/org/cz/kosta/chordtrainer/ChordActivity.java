package org.cz.kosta.chordtrainer;

import android.os.Bundle;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.widget.Toast;

public class ChordActivity extends Activity {

	private SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chord);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_chord, menu);
		this.db = new DatabaseHelper(this).getReadableDatabase();
		this.inform_about_controlls();
		return true;
	}

	
	public void inform_about_controlls(){
		Toast.makeText(this, "Touch to start or pause", Toast.LENGTH_LONG);
	}
	
}
