package org.cz.kosta.chordtrainer;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME="chords";
	public static final String CHORD_TITLE="chord_title";
	
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
		
	}

	private void createTable(SQLiteDatabase db){
		String sql = "CREATE TABLE chords (_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT);";
		db.execSQL(sql);
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		this.createTable(db);
		this.insert_standart_chords(db);
	}
	
	private void insert_standart_chords(SQLiteDatabase db){
		ContentValues cv =new  ContentValues();
		String[] chords = {"G", "A", "E", "Em"};
		for (String chord : chords) {
			cv.put(CHORD_TITLE, chord);
			db.insert("chords", CHORD_TITLE, cv);
		}
		
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS chords");
		this.onCreate(db);

	}

}
